
angular.module('linkflow', []);


$(function() {
  'use strict';

  var repo = {
      save: function (data, callback) {
        chrome.storage.sync.set(data, callback);
      },
      get: function (callback) {
        chrome.storage.sync.get(callback);
      }
  };


  repo.get(function (data) {
      $('#urlIgnore').val(data.urlIgnore);
  });

  $('#saveOptions').click(function () {
    var obj = {
      urlIgnore: $('#urlIgnore').val()
    };

    repo.save(obj, function() {
      alert("done dude!");
    });

  });

});

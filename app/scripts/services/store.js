angular.module('linkflow').factory('store', [function() {

  return {
      save: function (data, callback) {
        chrome.storage.sync.set(data, callback);
      },
      get: function (callback) {
        chrome.storage.sync.get(callback);
      }
  };

}]);

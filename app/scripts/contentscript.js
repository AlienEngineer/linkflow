/*global chrome: true, $: true, document: true, window: true*/

var DEFAULT_OPTIONS = {
	elements: [
		{
			selector: 'img',
			toTest: [{
				attr: 'src',
				method: 'GET'
			}]
		},
		{
			selector: 'a',
			toTest: [{
				attr: 'href',
				method: 'GET'
			}]
		}
	]
};


var Resolver = function (elements, port) {
	'use strict';
	this.options = elements;
	this.pagesFound = [window.location.pathname];
	this.port = port;
	this.visited = {};

	this.container = $('<div>').css('visibility', 'hidden');
	$(document.body).append(this.container);
};

Resolver.prototype.resolve = function (val, method, elem, elemOptions) {
	'use strict';
	var current = this;
	$.ajax({
		async: false,
		method: method,
		url: val
	}).success(function () {
		
		if (!current.visited[val]) {
			current.visited[val] = true;
			current.pagesFound.push(val);
		}
		
		current.port.postMessage({url: val, method: method, passed: true, page: current.currentPage, selector: elemOptions.selector});
		elem.css('color', 'green');
	}).fail(function () {
		current.port.postMessage({url: val, method: method, passed: false, page: current.currentPage, selector: elemOptions.selector});
		elem.css('color', 'red');
	});
};

Resolver.prototype.testElementsOfGivenType  = function (elementOption) {
	'use strict';
	var elementsToTest = this.container.find(elementOption.selector),
		current = this;
	elementsToTest.each(function () {
		var elem = $(this);
		elementOption.toTest.forEach(function (toTest) {
			current.resolve(elem.attr(toTest.attr), toTest.method, elem, elementOption);
		});
	});
};

Resolver.prototype.resolvePage = function (page) {
	'use strict';
	var current = this;
	current.container.empty();
	
	$.ajax({
		async: false,
		method: 'GET',
		url: page
	}).success(function (data) {
		
		if (!data) {
			return;
		}
		
		current.container.append(data);
		current.options.forEach(function (elementOptions) {
			current.testElementsOfGivenType(elementOptions);
		});
	});
	
};

Resolver.prototype.startTest = function () {
	'use strict';
		
	while (this.pagesFound.length) {
		this.currentPage = this.pagesFound.pop();
		this.resolvePage(this.currentPage);
	}
	
	this.port.postMessage({endTest: true});
};

chrome.runtime.onConnect.addListener(function (port) {
	'use strict';
	port.onMessage.addListener(function (msg) {
		if (msg === 'startTest') {
			var resolver = new Resolver(DEFAULT_OPTIONS.elements, port);
			resolver.startTest();
		}
	});
});

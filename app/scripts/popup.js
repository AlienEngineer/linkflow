/*global chrome: true, $: true, document: true*/
(function () {
	'use strict';
	
	var startBtn = document.getElementById('startBtn');
	
	startBtn.onclick = function () {
		chrome.tabs.getSelected(null, function (tab) {
			var port = chrome.tabs.connect(tab.id);
			port.postMessage('startTest');
			port.onMessage.addListener(function (msg) {
				
				console.log(JSON.stringify(msg));
				
				if (msg.endTest) {
					console.log('Test ended...');
					return port.disconnect();
				}
				
				var li = $('<li>').addClass('collection-item'),
					result = $('<p>').text(msg.passed ? 'Passed' : 'Failed').css('color', msg.passed ? 'green' : 'red'),
					tests = $('<p>').text(msg.method + ' ' + msg.url),
					selector = $('<p>').addClass('caption').text('Selector: ' + msg.selector),
					page = $('<p>').addClass('caption').text('Page: ' + msg.page);
				
				
				li
					.append(result)
					.append(tests)
					.append(selector)
					.append(page);
				
				$('#reportCollection').append(li);
			});
		});
	};
	
}());
